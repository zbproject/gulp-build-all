var gulp = require("gulp");
var webpack = require('webpack-stream');
gulp.task("default", function () {
    return gulp.src('./test.js')       // 路径问题：gulpfile.js为路径的起点。此路径表示js文件下的所有js文件。
        .pipe(webpack({
            // Any configuration options...
        }))
        .pipe(gulp.dest('dist/'));
});